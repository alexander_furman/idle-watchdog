#!/bin/bash

SETUP_PATH=/opt/idle-watchdog/idle-watchdog.py

if [ "$EUID" -ne 0 ]
  then echo "Please run as root!"
  exit
fi

if ! command -v python2.7 &>/dev/null; then
    echo "Python 2.7 is required!"
    exit
fi

if [ -e "$SETUP_PATH" ]; then
    echo "Idle watchdog is already installed!"
    exit
fi

mkdir -p /opt/idle-watchdog/
cp ./idle-watchdog.py $SETUP_PATH

sed -i -e '$i \python /opt/idle-watchdog/idle-watchdog.py &\n' /etc/rc.local
python /opt/idle-watchdog/idle-watchdog.py &

echo "export TMOUT=1800" >> ~/.profile

echo "Done. Idle watchdog is installed."
