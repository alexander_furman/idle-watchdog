This simple script allows to automatically turn off idle Linux machines. Idle in this case means no users logged in. 

It may be useful in Amazon EC2. Once installed it may save a lot of money by turning off unattended running instaces.  By default it powers off machines with idle time 30 minutes.

# Install
```bash
    > git clone git@bitbucket.org:alexander_furman/idle-watchdog.git
    > sudo ./setup.sh
```
# Log file
/var/log/idle-watchdog.log

# Stop watchdog
```bash
    > sudo kill $(cat /var/run/idle-watchdog.pid)
```