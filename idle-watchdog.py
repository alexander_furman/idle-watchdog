import os, sys
import subprocess
import re
import time
from datetime import datetime


logfile = "/var/log/idle-watchdog.log"
pidfile = "/var/run/idle-watchdog.pid"
check_interval_seconds = 30
timeout_minutes = 30

pid = os.fork()

if (pid == 0):
    logger = open(logfile, "a", 0)
    idle_since = 0
    
    while True:
        users_output = subprocess.check_output("users").strip()
        users = filter(None, re.split("\s+", users_output))
        
        if not users:
            if idle_since == 0:
                idle_since = datetime.utcnow()
            idle_time = int(round((datetime.utcnow() - idle_since).total_seconds()))
            if idle_time >= timeout_minutes * 60:
                logger.write("Idle time, seconds: {0}, since: {1}\n".format(idle_time, idle_since))
                logger.write("Idle timeout exceeded. Shutting down...\n")
                os.system("shutdown -P now")
        else:
            idle_since = 0
            
        time.sleep(check_interval_seconds)    
    
    logger.close()
else:
    pid_file = open(pidfile, "w", 0)
    pid_file.write(str(pid))
    pid_file.close()
    sys.exit(0)